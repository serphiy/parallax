/* global Vimeo */
'use strict';

import Rellax from 'rellax';

let container, parallax, sections, spacer, youImg, player;
let rellaxes = [];

function debounce(fn, ms) {
    let timeoutID;
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function() {
            fn.apply(context, args);
            timeoutID = null;
        }, ms);
    };
}

function getElementRect(el) {
    let rect = null;
    try {
        rect = el.getBoundingClientRect();
    } catch (err) {}
    return rect || {top: 0, bottom: 0, left: 0, right: 0, width: 0, height: 0};
}

function checkPassiveSupport() {
    let supportsPassive = false;
    try {
        var opts = Object.defineProperty({}, 'passive', {
            get: function() {
                supportsPassive = true;
                return null;
            }
        });
        window.addEventListener('testPassive', null, opts);
        window.removeEventListener('testPassive', null, opts);
    } catch (e) {}
    return supportsPassive;
}

let currentPercent = 0;
function bindMoveLayout(fn) {
    let isRunning, isThrottle, throttleTimer, context, args;
    let throttleDelay = 0;
    const run = function() {
        currentPercent = args[0] || 0;
        fn.apply(context, args);
        isRunning = false;
    };
    const skip = function() {
        currentPercent = args[0] || 0;
    };
    return function() {
        context = this;
        args = arguments;
        if (isRunning) {
            skip();
            return;
        }
        isRunning = true;
        setTimeout(function() {
            window.requestAnimationFrame(run);
            if (isThrottle) {
                clearTimeout(throttleTimer);
                throttleTimer = setTimeout(function() {
                    isThrottle = false;
                    throttleDelay = 0;
                }, throttleDelay + 16 * 2);
            }
            isThrottle = true;
            throttleDelay += 16 * 3;
        }, throttleDelay > 160 ? 160 : throttleDelay);
    };
}

function moveLayout(xPercent) {
    xPercent = currentPercent || xPercent;
    xPercent = Math.round(xPercent * 1000) / 1000;
    parallax.style.transform = `translate3d(${xPercent}%, 0, 0)`;
    const angle = (xPercent * sections.length / 100) * 72 + 216;
    if (youImg) {
        youImg.style.transform = `translate3d(0,0,0) rotate(${angle}deg)`;
    }
}

const moveLayoutSmooth = bindMoveLayout(moveLayout);

let lastScrollLeft = 0;
function onScroll(event) {
    if (!event) return;
    const target = event.currentTarget || event.target;
    if (!target) return;
    if (target.scrollLeft !== lastScrollLeft) {
        lastScrollLeft = target.scrollLeft;
        const ratio = target.scrollLeft / (target.scrollWidth);
        moveLayoutSmooth(-ratio * 100);
    }
}

let currentDiff = 0;
function bindScrollChange(fn) {
    let isRunning, context, args;
    const run = function() {
        fn.apply(context, args);
        currentDiff = 0;
        isRunning = false;
    };
    const skip = function() {
        currentDiff += args[1] || 0;
    };
    return function() {
        context = this;
        args = arguments;
        if (isRunning) {
            skip();
            return;
        }
        isRunning = true;
        window.requestAnimationFrame(run);
    };
}

function changeScrollPos(element, distance) {
    distance += currentDiff;
    element.scrollLeft += distance;
}

const changeScrollSmooth = bindScrollChange(changeScrollPos);

function onWheel(event) {
    if (!event) return;
    const target = event.currentTarget || event.target;
    if (!target) return;
    const PIXEL_STEP  = 10;
    const LINE_HEIGHT = 40;
    const PAGE_HEIGHT = 800;

    let sX = 0, sY = 0,       // spinX, spinY
        pX = 0, pY = 0;       // pixelX, pixelY

    // Legacy
    if ('detail'      in event) { sY = event.detail }
    if ('wheelDelta'  in event) { sY = -event.wheelDelta / 120 }
    if ('wheelDeltaY' in event) { sY = -event.wheelDeltaY / 120 }
    if ('wheelDeltaX' in event) { sX = -event.wheelDeltaX / 120 }

    // side scrolling on FF with DOMMouseScroll
    if ( 'axis' in event && event.axis === event.HORIZONTAL_AXIS ) {
        sX = sY;
        sY = 0;
    }

    pX = sX * PIXEL_STEP;
    pY = sY * PIXEL_STEP;

    if ('deltaY' in event) { pY = event.deltaY }
    if ('deltaX' in event) { pX = event.deltaX }

    if ((pX || pY) && event.deltaMode) {
        if (event.deltaMode == 1) {          // delta in LINE units
            pX *= LINE_HEIGHT;
            pY *= LINE_HEIGHT;
        } else {                             // delta in PAGE units
            pX *= PAGE_HEIGHT;
            pY *= PAGE_HEIGHT;
        }
    }

    const deltaPixels = pY || pX;
    if (deltaPixels) {
        event.preventDefault();
        changeScrollSmooth(target, deltaPixels);
    }
}

function onResize(event) {
    container.classList ? container.classList.add('resize') : container.className + ' resize';
    const ratio = container.scrollLeft / container.scrollWidth;
    spacer.style.width = '';
    spacer.style.height = '';
    const rect = getElementRect(parallax);
    spacer.style.width = `${rect.width}px`;
    spacer.style.height = `${rect.height * sections.length}px`;
    container.scrollLeft = ratio * container.scrollWidth;
    currentPercent = 0;
    moveLayout(-ratio * 100);
    for (let i = 0; i < rellaxes.length; i++) {
        rellaxes[i].refresh();
    }
    container.classList ? container.classList.remove('resize') : container.className.replace(/\bresize\b/gi, '');
}

const deboncedOnResize = debounce(onResize, 4);

function navigate(hash) {
    const id = hash.replace(/#(.*)/gi, '$1');
    if (!id) {
        container.scrollLeft = 0;
        return;
    }
    const destination = document.getElementById(id);
    if (!destination) return;
    const destX = destination.offsetLeft;
    container.scrollLeft = destX;
}

function handleNavClick(event) {
    const isModifiedEvent = function (event) {
        return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
    };
    if (!event || !event.target) return;
    let target = event.target;
    if (target.nodeName.toLowerCase() !== 'a' && target.nodeName.toLowerCase() !== 'button') {
        target = target.closest('A') || target.closest('BUTTON');
    }
    if (!target) return;
    if (target.nodeName.toLowerCase() === 'a') {
        const linkTarget = target.getAttribute('target');
        const href = target.getAttribute('href');
        if (
            !event.defaultPrevented && // onClick prevented default
            event.button === 0 && // ignore everything but left clicks
            (!linkTarget || linkTarget === '_self') && // let browser handle "target=_blank" etc.
            !isModifiedEvent(event) // ignore clicks with modifier keys
        ) {
            if (
                href && href.indexOf('#') !== -1 && 
                ((target.pathname === window.location.pathname) || ('/' + target.pathname === window.location.pathname))
            ) {
                event.preventDefault();
                navigate(href === '#' ? '#' : target.hash);
            }
        }
    }
    if (target.nodeName.toLowerCase() === 'button') {
        let step = 0;
        if (target.className.indexOf('arrow-prev') !== -1) {
            step -= 1;
        } else if (target.className.indexOf('arrow-next') !== -1) {
            step += 1;
        }
        if (step) {
            const section = target.closest(`.${parallax.className.split(/\s+/gi).join('.')} > section`);
            if (section) {
                const index = Array.prototype.indexOf.call(sections, section);
                if (
                    index !== -1 &&
                    ((step < 0 && (index + step) >= 0) || (step > 0 && (index + step) < sections.length))
                ) {
                    const destX = sections[index + step].offsetLeft;
                    container.scrollLeft = destX;
                }
            }
        }
    }
}

function initPolyfills() {
    let targetTime = 0;
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        function(callback){
            const currentTime = +new Date;
            const nextTime = Math.max(targetTime + 16, currentTime);
            const timeoutCb = function() { callback(targetTime = nextTime) };
            return setTimeout(timeoutCb, nextTime - currentTime);
        };
    }
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || 
                                    Element.prototype.webkitMatchesSelector;
    }
    if (!Element.prototype.closest) {
        Element.prototype.closest = function(s) {
            let el = this;
            do {
                if (el.matches(s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);
            return null;
        };
    }
}

function startPlayer(event) {
    const target = event && (event.currentTarget || event.target);
    if (!target) return;
    const playerContainer = document.getElementById('player');
    if (!playerContainer || !playerContainer.dataset || !playerContainer.dataset.url || !('Vimeo' in window) || !Vimeo.Player) return;
    target.removeEventListener('click', startPlayer);
    playerContainer.classList ? playerContainer.classList.add('ready') : playerContainer.className + ' ready';
    player.play();
}

function initPlayer() {
    const playerContainer = document.getElementById('player');
    const poster = document.getElementById('poster');
    const play = document.getElementById('play');
    if (!playerContainer || !playerContainer.dataset || !playerContainer.dataset.url || (!poster && !play) || !('Vimeo' in window) || !Vimeo.Player) return;
    poster && poster.addEventListener('click', startPlayer);
    play && play.addEventListener('click', startPlayer);
    const url = playerContainer.dataset.url;
    const options = {
        url,
        title: false,
        byline: false,
        portrait: false,
        transparent: true
    };
    player = new Vimeo.Player(playerContainer, options);
}

function init() {
    initPolyfills();
    initPlayer();
    const supportsPassive = checkPassiveSupport();
    container = document.getElementById('parallax-container');
    parallax = document.getElementById('parallax-list');
    sections = parallax.children;
    youImg = document.getElementById('you-img');

    spacer = document.createElement('div');
    spacer.className = 'spacer';
    spacer.style.position = 'relative';

    const rect = getElementRect(parallax);
    container.style.position = 'fixed';
    parallax.style.position = 'fixed';
    parallax.style.top = '0px';
    parallax.style.left = '0px';
    parallax.style.pointerEvents = 'none';
    parallax.style.zIndex = 2;
    spacer.style.width = `${rect.width}px`;
    spacer.style.height = `${rect.height * sections.length}px`;
    spacer.style.zIndex = 1;
    container.appendChild(spacer);
    container.style.overflowY = 'hidden';
    if (youImg) {
        youImg.style.transform = `translate3d(0,0,0) rotate(360deg)`;
    }

    container.addEventListener('scroll', onScroll, supportsPassive ? { passive: true } : false);
    container.addEventListener('wheel', onWheel, supportsPassive ? { passive: false } : false);
    window.addEventListener('resize', deboncedOnResize, supportsPassive ? { passive: true } : false);
    document.addEventListener('click', handleNavClick);

    for (let i = 0; i < sections.length; i++) {
        const layers = sections[i].children;
        for (let j = 0; j < layers.length; j++) {
            const rellax = new Rellax(layers[j], {
                round: true,
                horizontal: true,
                vertical: false,
                center: true,
                wrapper: container
            });
            rellaxes.push(rellax);
        }
    }
}

init();
