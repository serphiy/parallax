DOD-Interface Parallax
======================

[![parallax](screenshot.png)](screenshot.png)


Install
-------
run `npm install` in project directory


Development
-----------
run `gulp` or `npm run gulp` to build the project, start watcher and start local server


Build
-----
run `gulp build` or `npm run build` to build the project (not minified)

OR

run `npm run prod` to build and minify the project
